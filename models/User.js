// Require mongoose
const mongoose = require("mongoose");

// Create a schema design
const userSchema = new mongoose.Schema(
	{
		firstName: {
			type: String,
			required: [true, "First name is required"]
		},
		lastName: {
			type: String,
			required: [true, "Last name is required"]
		},
		address: {
			type: String,
			required: [true, "Address is required"]
		},
		email: {
			type: String,
			required: [true, "Email is required"]
		},
		password: {
			type: String,
			required: [true, "Password is required"]
		},
		paymentOption: {
			type: String,
			required: [true, "Payment option is required"]
		},
		isAdmin: {
			type: Boolean,
			default: false
		},
		purchases: [
			{
				productId: {
					type: String,
					required: [true, "Order ID is required"]
				},
				price: {
					type: Number,
					required: [true, "Price is required"]
				},
				quantity: {
					type: Number,
					required: [true, "Quantity is required"]
				},
				totalAmount: {
					type: Number
					//required: [true, "totalAmount is required"]
				},
				orderedOn: {
					type: Date,
					default: new Date
				}
			}
		]
	}
)

// Export to another file 
module.exports = mongoose.model("User", userSchema);