// Require mongoose
const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	
	customerId: {
				type: String,
				//required: [true, "User Id is required"]
			},

	productId: {
			type: String,
			required: [true, "Product Id is required"]
		},
	price: {
			type: Number,
			required: [true, "Price is required"]
		},
	quantity: {
			type: Number,
			required: [true, "Quantity is required"]
		},
	totalAmount: {
			type: Number,
			//required: [true, "Total Amount is required"]
		},
	purchasedOn: {
			type: Date,
			default: new Date
		}	
})
// Export to another file
module.exports = mongoose.model("Order", orderSchema);