// Require mongoose
const mongoose = require("mongoose");

const deliverySchema = new mongoose.Schema({
	isDelivered: {
			type: Boolean,
			default: false
	},

	customerId: {
			type: String
				//required: [true, "User Id is required"]
			},
	address: {
			type: String
	},

	paymentOption: {
			type: String
	},

	productId: {
			type: String,
			required: [true, "Product Id is required"]
		},
	price: {
			type: Number,
			required: [true, "Price is required"]
		},
	quantity: {
			type: Number,
			required: [true, "Quantity is required"]
		},
	totalAmount: {
			type: Number,
			//required: [true, "Total Amount is required"]
		},
	purchasedOn: {
			type: Date,
			default: new Date
		}
})

// Export to another file
module.exports = mongoose.model("Delivery", deliverySchema);