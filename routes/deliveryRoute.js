// Require express
const express = require("express");
// Create a router
const router = express.Router();

// Export the user controller
const deliveryController = require("../controllers/deliveryController");

// Export auth.js for user verification
const auth = require("../auth")

// ADDITIONAL FEATURE

// Get authenticated User's Deliveries (Non-admin)
router.get("/check-user-deliveries/", auth.verify, (req, res)=>{
	let data = {
		userId: auth.decode(req.headers.authorization).id,
	}
	console.log(data)

	if(data.userId){
		deliveryController.getVerifiedUserDeliveries(req.body)
		.then(result=>{
			res.send(result)
		})
	}else{
		res.send(false)
	}
})

// Get all customer orders (Admin only)
router.get("/all", auth.verify, (req, res)=>{
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	console.log(data)

	if(data.isAdmin){
		deliveryController.getAllDeliveries(data)
		.then(result=>{
			res.send(result)
		})
	}else{
		res.send(false)
	}
})

// Delete a product (Authenticated Admin)
router.delete("/:deliveryId/deleted", auth.verify, (req, res)=>{
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	console.log(data)
	if(data.isAdmin){
		deliveryController.deleteDelivery(req.params)
		.then(result=>{
			res.send(result)
		})
	}else{
		res.send(false)
	}
})

// Change isDelivery Status if delivered (Authenticated Admin)
router.put("/:deliveryId/status-updated", auth.verify, (req, res)=>{
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	console.log(data)
	if(data.isAdmin){
		deliveryController.deliveryStatus(req.params)
		.then(result =>{
			res.send(result)
		})
	}else{
		res.send(false)
	}
	
})

module.exports = router;