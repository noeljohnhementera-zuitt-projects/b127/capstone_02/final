// Require express
const express = require("express");
// Create a router
const router = express.Router();

//Export auth.js
const auth = require("../auth")

// Export the user controller
const userController = require("../controllers/userController");

// Register a User
router.post("/register", (req, res)=>{
	userController.createUser(req.body)
	.then(userCreated =>
		res.send(userCreated))
})

// Create a User Authentication
router.post("/login", (req, res)=>{
	userController.customerLogin(req.body)
	.then(result=>{
		res.send(result)
	})
})

// Set User as Admin
router.put("/:userId/status-updated", auth.verify, (req, res)=>{
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	console.log(data)
	if(data.isAdmin){
		userController.makeUserAdmin(req.params)
		.then(result =>{
			res.send(result)
		})
	}else{
		res.send(false)
	}
	
})

// Create Order (Authenticated User)
router.post("/order-successful", auth.verify, (req, res)=>{

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		address: auth.decode(req.headers.authorization).address,
		paymentOption: auth.decode(req.headers.authorization).paymentOption,
		productId: req.body.productId,
		price: req.body.price,
		quantity: req.body.quantity,
		totalAmount: req.body.quantity * req.body.price
	}
	console.log(data)

	if(data.userId){
		userController.buyProduct(data)
			.then(orders =>
				res.send(orders))
	}else{
		res.send(false)
	}
})

/*---------------------*/
// ADDITIONAL FEATURES

// Get All Users (No-authentication)
router.get("/all", (req, res)=>{
	userController.getAllUsers()
	.then(result =>{
		res.send(result);
	})
})

// Get a Single User (No-authentication)
router.get("/:userId", (req, res)=>{
	userController.getOneUser(req.params)
	.then(result=>{
		res.send(result)
	})
})

// Update Authenticated User Details (Authenticated Admin only)
router.put("/:userId/updatedSucessfully", auth.verify, (req, res)=>{
	let data = {
		userId: auth.decode(req.headers.authorization).id
	}
	if(data.userId){
		userController.updateUserInfo(req.params, req.body)
		.then(result=>{
			res.send(result)
		})
	}else{
		res.send(false)
	}
})

// Delete a user
router.delete("/:userId/deleted", auth.verify, (req, res)=>{
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	console.log(data)
	if(data.isAdmin){
		userController.deleteUser(req.params)
		.then(result=>{
			res.send(result)
		})
	}else{
		res.send(false)
	}
})

// Check if email exixts
router.post("/checkEmail", (req, res)=>{
	userController.checkEmailExists(req.body)
	.then(result => {
		res.send(result);
	});
})

module.exports = router;
/*// Create Order (Authenticated User - Admin Only)
router.post("/admin/orderSuccessful", auth.verify, (req, res)=>{

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		customerId: req.body.customerId,
		productId: req.body.productId,
		price: req.body.price,
		quantity: req.body.quantity,
		totalAmount: req.body.quantity * req.body.price
	}
	console.log(data)

	if(data.userId){
		userController.buyProduct(data)
			.then(orders =>
				res.send(orders))
	}else{
		res.send(false)
	}
	
})*/

/*// Create Order (Non-Admin)
router.post("/non-admin/orderSuccessful", auth.verify, (req, res)=>{

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		customerId: req.body.customerId,
		productId: req.body.productId,
		price: req.body.price,
		quantity: req.body.quantity,
		totalAmount: req.body.quantity * req.body.price
	}
	console.log(data)

	if(data.userId){
		userController.buyProduct(data)
			.then(orders =>
				res.send(orders))
	}else{
		res.send(false)
	}
	
})*/
