// Export the product schema model
const Product = require("../models/Product");

// Create a Product (Admin-only)
module.exports.createProduct = (info)=>{				
	
		let newProduct = new Product({
			name: info.product.name,
			description: info.product.description,
			price: info.product.price
		})
		return newProduct.save()
		.then((result, error)=>{
			if(error){
				return false
			}else{
				return result
			}
		})
}

// Retrieve All Active Products
module.exports.getAllActiveProducts = () =>{
	return Product.find({isActive: true})
	.then(result=>{
		return result
	})
}

// Retrieve a Single Product
module.exports.specificProduct = (idParam)=>{
	return Product.findById(idParam.productId)
	.then(result=>{
		return result
	})
}

// Update Product Information
module.exports.updateProduct = (productParams, reqBody) =>{
	let updateProductInfo = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	return Product.findByIdAndUpdate(productParams.productId, updateProductInfo)
	.then((updated, err)=>{
		if(err){
			return false
		}else{
			return updated
		}
	})
}

// Archiving a Product using an Admin User
module.exports.archiveProduct = (productParams) =>{
	let archived = {
		isActive: false
	}
	return Product.findByIdAndUpdate(productParams.productId, archived)
	.then((archived, err)=>{
		if(err){
			return false
		}else{
			return archived
		}
	})
}

// ADDITIONAL FEATURE

// Delete a product (Authenticated Admin)
module.exports.deleteProduct = (productParams) =>{
	return Product.findByIdAndRemove(productParams.productId)
	.then(result=>{
		return result
	})
}