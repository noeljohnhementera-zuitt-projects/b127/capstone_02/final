// Export the schema models
const Delivery = require("../models/Delivery");

// ADDITIONAL FEATURE

// Get authenticated User's Order (Non-admin)
module.exports.getVerifiedUserDeliveries = (orderParams)=>{
	return Delivery.find({customerId: orderParams.customerId})
	.then(result=>{
		return result
	})
}

// Get All Orders (Admin Only)
module.exports.getAllDeliveries = (data)=>{
	return Delivery.find({})
	.then(result=>{
		return result
	})
}
// Delete a product (Authenticated Admin)
module.exports.deleteDelivery = (deliveryParams) =>{
	return Delivery.findByIdAndRemove(deliveryParams.deliveryId)
	.then(result=>{
		return result
	})
}

// Change isDelivery Status if delivered (Authenticated Admin)
module.exports.deliveryStatus = (userParams)=>{

	let updatedStatus = {
		isDelivered: true
	}
	return Delivery.findByIdAndUpdate(userParams.deliveryId, updatedStatus)
	.then((updated, err)=>{
		if(err){
			return false
		}else{
			return true
		}
	})
}