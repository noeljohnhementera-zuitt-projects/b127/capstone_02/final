// Export the schema models
const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const Delivery = require("../models/Delivery");

// Add bcrypt and auth later 
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Register a User
module.exports.createUser = (enterDetails) =>{
	let newUser = new User ({
		firstName: enterDetails.firstName,
		lastName: enterDetails.lastName,
		address: enterDetails.address,
		email: enterDetails.email,
		password: bcrypt.hashSync(enterDetails.password, 10), 
		paymentOption: enterDetails.paymentOption
	})
	return User.find( {email: enterDetails.email} )		// I can't detect if the password if already existed in the database kapag encrypted sya
		.then(result=>{										// If no encryption, detectable sya
		if(result.length > 0){
			return `Email ${enterDetails.email} is already registered. Please choose a different email!`
		}else{
			return newUser.save()
			.then((result, error)=>{
				if(error){
					return false;
				}else{
					return result
				}
			})
		}
	})
}

// Create a User Authentication
module.exports.customerLogin = (enterLoginDetails)=>{
	return User.findOne({email: enterLoginDetails.email})
	.then(result =>{
		if(result === null){
			return false
		}else{
			const isPasswordSame = bcrypt.compareSync(enterLoginDetails.password, result.password)
				if(isPasswordSame){
					console.log(`Is Password Same? ${isPasswordSame}`)
					return { customerAccessToken: auth.createCustomerAccessToken(result.toObject())} // gagawan ng token ang laman ng data sa auth.js
				}else{
					return false
				}
		}
	})
}

// Set User as Admin
module.exports.makeUserAdmin = (userParams)=>{

	let updatedStatus = {
		isAdmin: true
	}
	return User.findByIdAndUpdate(userParams.userId, updatedStatus)
	.then((updated, err)=>{
		if(err){
			return false
		}else{
			return true
		}
	})
}

// Create Order (Authenticated User)
module.exports.buyProduct = async(data)=>{

	let isUserOrdered = await User.findById(data.userId)
	.then(user=>{
		user.purchases.push({
						  productId: data.productId,
						  price: data.price,
						  quantity: data.quantity, 
						  totalAmount: data.totalAmount
						})
		//let totalAmount = data.quantity * data.price
		return user.save()
		.then((result, err)=>{
			if(err){
				return false
			}else{
				return result
			}
		})
	})

	let isProductOrdered = await Product.findById(data.productId)
	.then(product=>{
		product.orders.push({userId: data.userId})
		return product.save()
		.then((result, err)=>{
			if(err){
				return false
			}else{
				return result
			}
		})
	})
	
	/*if(isUserOrdered && isProductOrdered){
		return true
	}else{
		return false
	}
	// For validation only if I want to push something sa array*/

	let newOrder = await new Order({
		customerId: data.userId,
		productId: data.productId,
		price: data.price,
		quantity: data.quantity, 
		totalAmount: data.totalAmount
	})

	return newOrder.save()
	.then((result, err)=>{
			if(err){
				return false
			}else{
				let newDelivery = new Delivery({
				customerId: data.userId,
				address: data.address,
				paymentOption: data.paymentOption,
				productId: data.productId,
				price: data.price,
				quantity: data.quantity, 
				totalAmount: data.totalAmount
			})

			return newDelivery.save()
			.then((result, err)=>{
					if(err){
						return false
					}else{
						return "User's Order and Delivery Successfully Created!"
					}
				})
			}
		})
}

/*---------------------*/
// ADDITIONAL FEATURES

// Get all users
module.exports.getAllUsers = () =>{
	return User.find({})
	.then(result =>{
		return result;
	})
}
         
// Get a Single User
module.exports.getOneUser = (getId)=>{
	return User.findById(getId.userId)
	.then(result=>{
		return result
	})
}

// Update Authenticated User Details (Authenticated Admin only)
// Password is required since it uses bcypt. It requires data and salt value
module.exports.updateUserInfo = (getUserId, reqBody)=>{
	let newUserInfo = {
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		address: reqBody.address,
		email: reqBody.email, 
		password: bcrypt.hashSync(reqBody.password, 10),
		paymentOption: reqBody.paymentOption
	}
	return User.findByIdAndUpdate(getUserId.userId, newUserInfo)
	.then((updated, err)=>{
		if(err){
			return false
		}else{
			return updated
		}
	})
}

// Delete a user
module.exports.deleteUser = (userParams) =>{
	return User.findByIdAndRemove(userParams.userId)
	.then(result=>{
		return result
	})
}

// Check if email exixts
module.exports.checkEmailExists = (reqBody) => {
	return User.find( {email: reqBody.email} )
	.then(result =>{ // Gumawa lang ng if else statement to know if existing or not yung ilalagay sa req.body (Postman)
		if(result.length > 0){ // If it matches yung email sa database
			return "Email already exist";
		}else{
			return "Email does not exist"
		}
	})
}

/*// Create Order (Authenticated User - Admin Only)
module.exports.buyProduct = async(data)=>{

	let isUserOrdered = await User.findById(data.userId)
	.then(user=>{
		user.purchases.push({
						  productId: data.productId,
						  price: data.price,
						  quantity: data.quantity, 
						  totalAmount: data.totalAmount
						})
		//let totalAmount = data.quantity * data.price
		return user.save()
		.then((result, err)=>{
			if(err){
				return false
			}else{
				return result
			}
		})
	})

	let isProductOrdered = await Product.findById(data.productId)
	.then(product=>{
		product.orders.push({userId: data.userId})
		return product.save()
		.then((result, err)=>{
			if(err){
				return false
			}else{
				return result
			}
		})
	})
//	
	let isOrderPosted = await Order.findOne({customerId: data.customerId})
	.then(order=>{
		order.orderDetails.push({
								productId: data.productId,
						 	    price: data.price,
						  		quantity: data.quantity, 
						  		totalAmount: data.totalAmount
							})
		return order.save()
		.then((result, err)=>{
			if(err){
				return false
			}else{
				return result
			}
		})
	})

	if(isUserOrdered && isProductOrdered && isOrderPosted){
		return true
	}else{
		return false
	}
}*/

/*
// Create Order (Non-Admin)
module.exports.buyProduct = async(data)=>{

	let isUserOrdered = await User.findById(data.userId)
	.then(user=>{
		user.purchases.push({
						  productId: data.productId,
						  price: data.price,
						  quantity: data.quantity, 
						  totalAmount: data.totalAmount
						})
		//let totalAmount = data.quantity * data.price
		return user.save()
		.then((result, err)=>{
			if(err){
				return false
			}else{
				return result
			}
		})
	})

	let isProductOrdered = await Product.findById(data.productId)
	.then(product=>{
		product.orders.push({userId: data.userId})
		return product.save()
		.then((result, err)=>{
			if(err){
				return false
			}else{
				return result
			}
		})
	})
//	
	let isOrderPosted = await Order.findOne({customerId: data.customerId})
	.then(order=>{
		order.orderDetails.push({
								productId: data.productId,
						 	    price: data.price,
						  		quantity: data.quantity, 
						  		totalAmount: data.totalAmount
							})
		return order.save()
		.then((result, err)=>{
			if(err){
				return false
			}else{
				return result
			}
		})
	})

	if(isUserOrdered && isProductOrdered && isOrderPosted){
		return true
	}else{
		return false
	}
}
*/