// Export the schema models
const Order = require("../models/Order");
const User = require("../models/User");
const Product = require("../models/Product");

// Get authenticated User's Order (Non-admin)
module.exports.getVerifiedUserOrder = (orderParams)=>{
	return Order.find({customerId: orderParams.customerId})
	.then(result=>{
		return result
	})
}

// Get All Orders (Admin Only)
module.exports.getAllOrder = (data)=>{
	return Order.find({})
	.then(result=>{
		return result
	})
}

// ADDITIONAL FEATURE

// Delete a product (Authenticated Admin)
module.exports.deleteOrder = (orderParams) =>{
	return Order.findByIdAndRemove(orderParams.orderId)
	.then(result=>{
		return result
	})
}

/*----------------------------------------------------------*/
// NOT PART OF THE REQUIREMENT (Just Random Controllers hehe)
/*
// Set user as Admin
module.exports.makeUserAdmin = (customerParams)=>{

	let updatedStatus = {
		isAdmin: true
	}
	console.log(updatedStatus)
	return Order.findByIdAndUpdate(customerParams.customerId, updatedStatus)
	.then((updated, err)=>{
		if(err){
			return false
		}else{
			return true
		}
	})
}

// Crate authenticated User Id Order Transaction
module.exports.seeOrderDetails = async(data, orderParams)=>{

	let isUserOrdered = await Order.findById(orderParams)
	.then(order=>{
		order.orderDetails.push({
						  productId: data.productId,
						  price: data.price,
						  quantity: data.quantity, 
						  totalAmount: data.totalAmount
						})
		//let totalAmount = data.quantity * data.price
		return order.save()
		.then((result, err)=>{
			if(err){
				return false
			}else{
				return result
			}
		})
	})

	let isProductOrdered = await Product.findById(data.productId)
	.then(product=>{
		product.orders.push({userId: data.userId})
		return product.save()
		.then((result, err)=>{
			if(err){
				return false
			}else{
				return result
			}
		})
	})

	if(isUserOrdered && isProductOrdered){
		return true
	}else{
		return false
	}
}

// Get Single Order
module.exports.trackOrder = (userId)=>{
	return Order.findById(userId)
	.then((res, err)=>{
		if(err){
			return false
		}else{
			return res
		}
	})
}

// Add Authenticated User's orders
module.exports.getverifiedUserOrder = (user)=> {
	if(user.isAdmin){

		let verifiedCustomer = new Order ({
		customerId: user.product._id
		})
		return verifiedCustomer.save()
		.then((result, err)=>{
			if(err){
				return false
			}else{
				return result
			}
		})
	}else{
		return false
	}

}

// Get Authenticated User's orders
module.exports.addVerifiedUserOrder =(user, verifiedId)=>{
	if(user.isAdmin){
		return Order.findById(verifiedId)
		.then(result=>{
			return result
		})
	}else{
		return false
	}
}

// Get All orders (Admin User)
module.exports.getAdminUserOrder = ()=>{
	return Order.find({isAdmin: true})
	.then(result=>{
		return result
	})
}*/