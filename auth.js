// Declare jsonwebtoken
const jwt = require("jsonwebtoken");

// Declare a scret value
const secret = "MyAwesomeFoodDeliveryAPI";

// Access Token Creation
module.exports.createCustomerAccessToken = (user) =>{
	const data = {																	
		id: user._id,
		email: user.email,
		address: user.address,
		paymentOption: user.paymentOption,
		isAdmin: user.isAdmin
	};
	return jwt.sign(data, secret, {})
}


// Create a Token Verification or Authentication
module.exports.verify = (req, res, next) =>{		
 	let token = req.headers.authorization; 			
 	if(typeof token !== "undefined"){
 		console.log(token);

 	token = token.slice(7, token.length);

 	return jwt.verify(token, secret, (err, data) => {
 		if(err){
 			return res.send( {auth: "failed"} )
 		}else{
 			
 			next()
 		}		
 	})

 	}else{
 		return res.send( {auth: "failed"} );
 	}
}

// Create a Token Decryption or Decoding
module.exports.decode = (token) =>{			

 	if(typeof token !== "undefined"){ 
 		token = token.slice(7, token.length);

 		return jwt.verify(token, secret, (err, data) =>{

 			if(err){
 				return null;
 			}else{
 				return jwt.decode(token, {complete: true}).payload		
 			}													
 		})
 	}else{
 		return null;
 	}

}